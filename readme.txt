![images.jpg](https://bitbucket.org/repo/A4bdpB/images/2493427314-images.jpg)

Game Music Emulators
------------------------------------------
It is a collection of  music file [emulators](https://en.wikipedia.org/wiki/Emulator) for video game  that
support tmany formats and systems: like AY, ZX, GBS, GYM, HES, KSS, MSX Home Computer/other Z80 systems.

Very easy to use library and one set of common functions work well with with all emulators and has a [portable code](http://stackoverflow.com/questions/12583330/what-is-meant-when-a-piece-of-code-is-said-to-be-portable) which can be used on any system. The [business envelpes](https://www.businessenvelopes.com/) can be made for [special effects](https://en.wikipedia.org/wiki/Special_Effects_(album)). You can also read music data from file memory or custom function.